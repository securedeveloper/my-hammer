# MyHammer

## Installation
```bash
npm install
```

## Development server

```bash
ng serve
```

Update `showOnlyActiveJobs` flag to `false` under [my-hammer/src/app/app.component.ts](my-hammer/src/app/app.component.ts) to see full list of job (initially only active jobs are loaded)

Navigate to `http://localhost:4200/`

## Running unit tests

```bash
ng test
```

## My Approach
+ Angular Material as UI theme and components
+ Job service to fetch jobs from Api (in our case the JSON file in under `assets/jobs.json`)
+ Single App component to hold the application
  - Optimization will be dividing app to different component, holding `app-title-panel`, `app-job-list`, `app-job-details`
+ Unit tests
  -  unit test for `JobService` and `AppComponent`
  -  I believe the component could be tested more in different ways (Imagination is the limit)
  
## Other Approaches
To load  job details I could have use routing, but It did not make sense to go for routing solution when we have not enough component (application modules),

Feel free to ask me anything about approach, task @
- [securedeveloper@gmail.com](securedeveloper@gmail.com)
- [+436764892500](+436764892500)

