import {Component, OnInit} from '@angular/core';
import {Job, JobResponse} from './job.def';
import {JobsService} from './jobs.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [JobsService]
})

export class AppComponent implements OnInit {
  error: string; // track of error caused by Api call
  jobs: Job[] = []; // jobs fetched from Api
  currentJob: Job; // job item clicked for details
  showJobDetail: boolean; // flag to show/hide job details panel
  showOnlyActiveJobs: boolean; // flag to toggle between onlyActive/All jobs from Api (set to false to see all jobs)
  step: number; // step counter for accordion
  today: Date; // today date to display expired ribbon tag while showing job details
  title: string;

  constructor(private jobsService: JobsService, private snackBar: MatSnackBar) {
    this.title = 'MyHammer';
    this.showJobDetail = false;
    this.showOnlyActiveJobs = true;
    this.step = 0;
    this.today = new Date();
  }

  ngOnInit() {
    this.loadJobs();
  }

  loadJobs() {
    this.jobsService.getJobs().subscribe(this.handleResponse, this.handleError);
  }

  handleResponse = (response: JobResponse) => {
    if (!response.error) {
      this.jobs = this.loadFilteredJobs(response.body);

      return;
    }

    this.error = response.error;
  };

  handleError = (error: Error) => {
    this.error = error.message;
  };

  loadFilteredJobs(jobs: Job[]) {
    if (!this.showOnlyActiveJobs) {
      this.openSnackBar('Showing all jobs, change showOnlyActiveJobs flag to see only active jobs...', 'OK', 3000);
      return jobs;
    }

    this.openSnackBar('Showing only active jobs, change showOnlyActiveJobs flag to see all jobs...', 'OK', 3000);
    return (jobs || []).filter(job => job.state === 'active');
  }

  loadJobDetails(id: number) {
    if (!this.jobs || this.jobs.length === 0) {
      this.loadJobs();

      this.openSnackBar('No jobs found, loading again ...', 'OK');
      return;
    }

    this.currentJob = this.jobs.find(job => job.id === id);
    this.showJobDetail = true;
    // this.step = 0;
    this.openSnackBar(`Loading job details...`, '🌐', 500);
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  openSnackBar(message: string, action: string, timer?: number) {
    const durationTime: number = timer && timer > 0 ? timer : 2000;

    this.snackBar.open(message, action, {
      duration: durationTime,
    });
  }
}
