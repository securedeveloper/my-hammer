import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {HttpClientModule} from '@angular/common/http';
import {JobsService} from './jobs.service';
import {JobResponse} from './job.def';

const store: JobResponse = {
  errno: 0,
  error: '',
  body: [
    {
      id: 98969442,
      title: 'Job title',
      zip_code: 10115,
      city: 'Berlin',
      thumbnail: '//placekitten.com/150/150',
      attachments: [
        '//placekitten.com/300/200',
        '//placekitten.com/200/400',
        '//placekitten.com/250/250'
      ],
      counter: {
        'messages_total': 4,
        'messages_unread': 1
      },
      is_awarded: false,
      categories: [{id: 41}],
      state: 'active',
      description: 'Job description',
      end_date: new Date('2018-10-31T14:14:32+01:00'),
      created_at: new Date('2018-10-01T14:14:32+02:00')
    }
  ]
};

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let appComponent: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        HttpClientModule,
      ],
      providers: [JobsService]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    appComponent = fixture.componentInstance;
    appComponent.jobs = store.body;
  }));

  const getShallowInstance = () => fixture.debugElement.componentInstance;
  const getCompiledInstance = () => {
    fixture.detectChanges();

    return fixture.debugElement.nativeElement;
  };

  it('should create the app', () => {
    expect(getShallowInstance()).toBeTruthy();
  });

  it(`should have as title 'MyHammer'`, () => {
    expect(getShallowInstance().title).toEqual('MyHammer');
  });

  it(`should render a gavel icon`, () => {
    expect(getCompiledInstance().querySelector('.mh-logo').textContent).toEqual('gavel');
  });

  it('should render title in a h1 tag', () => {
    expect(getCompiledInstance().querySelector('h1').textContent).toBe('MyHammer');
  });

  it('should render jobs as list', () => {
    expect(getCompiledInstance().querySelector('.mh-jobs-list')).toBeTruthy();
    expect(getCompiledInstance().querySelector('.mh-job-item')).toBeTruthy();
  });

  it('should render job description on clicking', () => {
    spyOn(appComponent, 'loadJobDetails');
    expect(getCompiledInstance().querySelector('.mh-job-description')).toBeTruthy();
  });
});
