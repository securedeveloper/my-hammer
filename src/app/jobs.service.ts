import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {JobResponse} from './job.def';

@Injectable()
export class JobsService {
  private readonly jobsUrl;

  constructor(private http: HttpClient) {
    this.jobsUrl = '/assets/jobs.json';
  }

  getJobs(): Observable<JobResponse> {
    return this.http.get<JobResponse>(this.jobsUrl);
  }
}
