import {TestBed, async} from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {JobsService} from './jobs.service';
import {Job, JobResponse} from './job.def';

describe('JobService', () => {
  let jobService: JobsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({imports: [HttpClientModule], providers: [JobsService]}).compileComponents();

    jobService = TestBed.get(JobsService);
  }));

  it('should create the service', () => {
    expect(jobService).toBeTruthy();
    expect(jobService.getJobs()).toBeDefined();
  });

  it('should get the jobs', () => {
    let jobs: Job[] = [];
    const jobIds: string[] = ['98969442', '68934444', '38934412', '8969556'];

    jobService.getJobs().subscribe((response: JobResponse) => {
      jobs = response.body as Job[];

      jobs.forEach((job: Job, index: number) => {
        expect(`${job.id}`).toBe(jobIds[index]);
      });
    });
  });
});
