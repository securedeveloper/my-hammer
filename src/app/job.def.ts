interface JobCategory {
  id: number;
}

interface JobCounter {
  messages_total: number;
  messages_unread: number;
}

type JobStatus = 'inactive' | 'active';

export interface Job {
  id: number;
  title: string;
  zip_code: number;
  city: string;
  thumbnail: string;
  attachments: string[];
  counter: JobCounter;
  is_awarded: boolean;
  categories: JobCategory[];
  state: JobStatus;
  description: string;
  end_date: Date;
  created_at: Date;
}

export interface JobResponse {
  errno: number;
  error: string;
  body: Job[];
}
