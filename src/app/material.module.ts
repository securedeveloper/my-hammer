import {NgModule} from '@angular/core';

import {
  MatBadgeModule,
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatChipsModule,
  MatGridListModule,
  MatTabsModule,
  MatRippleModule,
  MatExpansionModule,
  MatSnackBarModule
} from '@angular/material';

const MaterialModules = [
  MatBadgeModule,
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatChipsModule,
  MatGridListModule,
  MatTabsModule,
  MatRippleModule,
  MatExpansionModule,
  MatSnackBarModule
];

@NgModule({
  imports: MaterialModules,
  exports: MaterialModules
})


export class MaterialModule {
}
